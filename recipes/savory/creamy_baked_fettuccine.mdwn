[[!meta title="Creamy Baked Fettuccine with Asiago"]]

Adapted from Giada de Laurentiis

3000 cal/recipe

## Ingredients

### Creme Fraiche

+ 16 oz heavy cream
+ 2 tbsp buttermilk

### Fettuccine

+ 1 lb dry fettuccine pasta
+ 2 c grated Asiago cheese, plus 1/4 cup (16 oz total)
+ 1 c grated Parmesan cheese
+ 1 1/2 tbsp fresh chopped thyme leaves (or 1 1/2 tsp dried thyme)
+ 1/2 tsp salt
+ 1/2 tsp freshly ground black pepper


## Directions

### Creme Fraiche

+ Combine buttermilk and heavy cream in a lidded, glass container.
+ Cover and allow to rest at room temperature until thickened (about 12 hours).
+ Store in the refrigerator for up to 2 weeks.

### Fettuccine

+ Preheat the oven to 375 degrees F.
+ Bring a large pot of salted water to a boil over high heat.
+ Add the pasta and cook until tender but still firm to the bite, stirring occasionally, about 8 to 10 minutes.
+ Drain pasta, reserving 1 cup of the liquid.
+ In a large bowl combine the 2 cups Asiago cheese, crème fraiche, Parmesan, thyme, salt, pepper, cooked pasta, and pasta liquid.
+ Gently toss until all the ingredients are combined and the pasta is coated.
+ Place the pasta in a buttered, 11 inch by 15 inch baking dish and sprinkle with the remaining 1/4 cup Asiago cheese.
+ Bake until golden on top, about 25 minutes.
+ Let sit for at least 5 minutes and serve. 

 

[[!tag cream cheese pasta]]
