[[!meta title="Spicy Baked Pasta with Chili and Broccoli Rabe"]]

Adapted from Alison Roman

4975 cal/recipe

## Ingredients

+ 1 lb rigatoni, ziti, or fusilli
+ 8 tbsp olive oil, divided
+ 3 large leeks, whites and pale greens only, halved lengthwise and thinly sliced into half-moons
+ 1 tsp salt and a pinch, divided
+ 1 tsp ground black pepper and a pinch, divided
+ 1 tsp crushed red pepper flakes
+ 2 bunches broccoli rabe or 3 bunches baby broccolini, trimmed and coarsely chopped
+ 1 c heavy cream
+ 12 oz grated sharp white cheddar (about 3 c), divided
+ 1 c chopped chives, divided
+ 1 c panko

## Directions

+ Preheat oven to 425 F.
+ Bring a large pot of salted water to boil.
+ Add pasta to boiling water and cook till barely done.
+ Reserve 1 c of pasta water and drain remainder.
+ Heat 5 tbsp olive oil in large cast iron skillet at med-high.
+ Cook leeks, salt, and pepper in skillet until softened and starting to brown. 
+ Add red pepper flakes to skillet.
+ Add broccoli rabe to skillet in small handfuls, stirring each batch in and allowing to wilt before adding more.
+ Once all rabe has been added, quick for a few minutes more until all are bright green and wilted.
+ Remove skillet from heat.
+ Add pasta, reserved pasta water, 9 oz cheese, and 1/2 c chopped chives to skillet and stir well.
+ Toss panko, pinch of salt, pinch of pepper, and 3 tbsp oil in small bowl.
+ Scatter panko over skillet.
+ Top panko with 3 oz cheese.
+ Bake pasta in oven until bubbly and pank is deep golden brown (about 30 min).
+ Serve topped with 1/2 c chives.

[[!tag cheese 'heavy cream' rabe leek]]
