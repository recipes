[[!meta title="Creamy Potato Fennel Souop"]]

Adapted from "Go-To Dinners" by Barefoot Contessa

2320 cal/recipe

## Ingredients

+ 3 tbsp olive oil
+ 2 tbsp unsalted butter
+ 2 bulbs (about 5 c sliced) fennel, tops and cores removed and fronds reserved
+ 3 yellow onions, sliced (about 4 c)
+ 1 1/2 lb Yukon Gold potatoes, 1-in dice
+ 5 c vegetable broth
+ 1 tbsp salt
+ 1 tsp ground black pepper
+ 2 1/2 tsp ground anise
+ 1 c half-and-half
+ 4 oz sharp white cheese, 1/2-in cube
+ 4 tbsp croutons

## Directions

+ Heat oil and butter in large pot over medium-low heat.
+ Add fennel and onions and saute until beginning to brown (about 15 min).
+ Add potatoes, broth, salt, pepper, and anise and bring to a boil.
+ Lower heat, cover, and simmer until potatoes are very tender (about 30 min).
+ Insert an immersion blender in the pot and purée until smooth.
+ Stir in half-and-half.
+ Reheat over medium-low heat until hot.
+ Place cheese and croutons in bowls and pour soup over top.
+ Garnish with reserved fennel fronds.

[[!tag fennel potato "half-and-half" croutons]]
