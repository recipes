[[!meta title="Wheat Bread"]]
## Ingredients
+ 10-12 oz water
+ 1.5 t salt
+ 1.5 T vegetable oil
+ 2 T molasses
+ 2 C bread flour
+ 2 C Whole Wheat Flour
+ 2 t active dry yeast
## Directions


[[!tag bread_maker wheat bread]]
