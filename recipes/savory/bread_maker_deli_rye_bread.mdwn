[[!meta title="Deli Rye Bread"]]
## Ingredients
+ 8 oz buttermilk
+ 1-2 oz water
+ 1.25 t salt
+ 1 egg
+ 1 T vegetable oil
+ 2 T molasses
+ 2 C bread flour
+ 1.5 C rye flour
+ 0.25 t baking soda
+ 2 t active dry yeast

## Directions

Put ingredients into bread maker; bake on whole wheat setting

[[!tag bread bread_maker rye]]
