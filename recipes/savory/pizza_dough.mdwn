[[!meta title="Bread Maker Pizza Dough"]]

2230 cal/recipe

## Ingredients

+ 1 1/4 c (283 g) water
+ 1 round tsp ground dry fennel
+ 1 1/2 tsp dry basil
+ 1 round tsp salt
+ 1/4 round tsp red pepper flakes
+ 1 round tsp powdered garlic
+ 3 tbsp olive oil
+ 4 c (488 g) all-purpose flour
+ 2 tsp active dry yeast

## Directions

+ Put ingredients in order listed into bread maker.
+ Run bread maker on dough setting.
+ Leave dough in bread maker until doubled in size.

## Note

Makes enough bread for two 12-inch deep dish pizzas or 2 16-inch thin crust pizzas.

[[!tag pizza dough "bread maker"]]
