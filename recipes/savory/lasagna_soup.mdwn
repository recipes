[[!meta title="Lasagna Soup"]]

4800 cal/recipe

## Ingredients

### Soup Ingredients
+ 1 lb Italian sausage (Hot works well)
+ 2 onions, chopped
+ 4 garlic cloves, minced
+ 2 t dried oregano
+ 1/3 t crushed red pepper
+ 10 T tomato paste
+ 28 oz can fire-roasted diced tomatoes
+ 2 bay leaves
+ 6 C chicken stock
+ 8 oz dry mafalda or fusilli pasta
+ 0.5 tsp salt
+ 0.5 t ground black pepper

### Cheese Lava Ingredients
+ 15 oz ricotta
+ 1 C grated parmesan cheese
+ 0.25 t salt
+ 0.125 t black pepper
+ 0.5 C finely chopped fresh basil
+ 1 C mozzarella

## Directions

+ Cook sausage half through in skillet
+ Transfer sausage and 2 t of sausage grease to soup pot
+ Add onions and saute until unions are softened
+ Add garlic, oregano, and red pepper; cook 1 min
+ Add tomato patse, stir well, and cook about 3 min to turn tomato rust-colored
+ Add diced tomatoes, bay leaves, and stock
+ Stir well, bring to a boil, and simmer for 30 min
+ In medium bowl, stir together cheese lava ingredients
+ Divide cheese lava among bowls
+ Add pasta to soup and cook until just *al dente*
+ Remove pasta from heat and divide among bowls immediately

[[!tag lasagna soup cheese]]
