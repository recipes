[[!meta title="World's Best Gingerbread"]]

Adapted from u/happieKampr on reddit/Old_Recipes (https://www.reddit.com/r/Old_Recipes/comments/qm9pf1/worlds_best_gingerbread/)

6270 cal/recipe

## Ingredients

+ 1 c butter, softened
+ 1/2 c packed brown sugar
+ 2 c blackstrap molasses
+ 1 c buttermilk
+ 2 tbsp dry ground ginger
+ 1 tbsp ground cinnamon
+ 1 tsp ground cloves
+ 1 tsp ground nutmeg
+ 1 tsp ground allspice
+ 3 eggs
+ 4 c all-purpose flour, sifted
+ 1 tsp baking soda

## Directions

+ Preheat oven to 350 F.
+ Grease a 9-inch by 13-inch pan or a gingerbread cake tin.
+ Cream butter and sugar.
+ Beat in molasses, buttermilk, and spices.
+ Beat in eggs, one at a time.
+ In small bowl, blend flour and baking soda.
+ Stir flour mixture into butter mixture until just incoporated.
+ Bake until a toothpick inserted in the center comes out without batter (about 45 min). 

[[!tag spice buttermilk molasses]]
