[[!meta title="White Chocolate Cranberry Crumb Bars"]]

Adapted from Broma Bakery (https://bromabakery.com/cranberry-crumb-bars/?utm_campaign=later-linkinbio-bromabakery&utm_content=later-31720830&utm_medium=social&utm_source=linkin.bio)

5855 cal/recipe

## Ingredients

### Crust Dough

+ 2/3 cup old fashioned oats
+ 2 1/2 cups all purpose flour
+ 2/3 cup granulated sugar
+ 2/3 packed cup brown sugar
+ 1 tsp baking soda
+ 3/4 tsp salt
+ 1 cup unsalted butter, cold and cut into small cubes
+ 1 egg, room temperature
+ 2 tbsp milk
+ 2 tsp vanilla extract 

### Cranberry Layer

+ 4 cups fresh whole cranberries
+ 1 cup granulated sugar
+ 2 tbsp flour
+ 1 tsp vanilla extract
+ 1 tbsp orange zest (zest of one orange)
+ 1/8 tsp salt

### White Chocolate Drizzle

+ 1/2 cup white chocolate chips, melted

## Directions

+ Preheat the oven to 350 °F.
+ Line a 9-inch by 9-inch pan with parchment paper on all sides.
+ In a large bowl combine the oats, flour, sugars, baking soda, and salt and then stir to combine.
+ Cut the butter cubes into the oat mixture until the mixture start to clump together and resembles wet sand.
+ Make a well in the center of the the dry ingredient and add the egg and milk.
+ Use a fork to beat the egg and milk together and stir together until it forms a dough ball.
+ Press about 2/3 of the mixture into an even layer in your prepared pan.
+ In a second large bowl combine all of the ingredients for the cranberry layer and toss to combine.
+ Spread the cranberries over the prepared crust`, patting into an even layer.
+ Sprinkle the remaining crust over the cranberries, patting it gently onto the cranberry layer.
+ Bake for 30 to 40 minutes or until the crust has puffed up and is golden brown around the edges and the cranberries are bubbling.
+ Allow to cool completely before drizzling with white chocolate and slicing.

## Note

[[!tag cranberry oat "white chocolate"]]
